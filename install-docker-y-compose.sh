#!/bin/bash

##actualizar kernel
versionkernel=`uname -r`

if [ `uname -r` != "$versionkernel" ]; then
yum update -y
else
echo "La version del kernel ya esta actualizado"
fi

##install docker
setenforce 0
systemctl stop firewalld
yum install epel-release -y
yum install docker vim -y 

##instalar docker-compose
yum install curl -y
curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
echo 'y' | yum install docker-compose -y

systemctl start docker

systemctl status docker

systemctl enable docker


